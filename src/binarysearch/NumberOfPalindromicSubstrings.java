package binarysearch;

import java.util.ArrayList;

public class NumberOfPalindromicSubstrings {
    public static void main(String[] args) {
        System.out.println(solve("abacvbava"));
    }

    public static int solve(String s) {
        int iCounter = 0;
        for (int i = 0; i < s.length(); i++) {
            for (String sword : words(s.substring(i))) {
                if (sword.equals(new StringBuilder(sword).reverse().toString())) {
                    iCounter++;
                }
            }
        }
        return iCounter;
    }

    public static ArrayList<String> words(String s) {
        ArrayList<String> aOutputs = new ArrayList<>();
        char cletter = s.charAt(0);
        int iPosition = 0;
        while (iPosition != -1) {
            aOutputs.add(s.substring(0, iPosition + 1));
            iPosition = s.indexOf(cletter, iPosition + 1);
        }
        return aOutputs;
    }
}
