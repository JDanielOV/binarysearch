package binarysearch;

public class SubsequenceMatchTarget {
    public static void main(String[] args) {
        //System.out.println(solve(new String[]{"acbc"},"acxbcadc"));
        System.out.println("abaca".indexOf('a', 3));
    }

    public static int solve(String[] words, String s) {
        int iOutput = 0;
        for (String word : words) {
            int iUltPostion = -1;
            boolean bControl = true;
            for (int i = 0; i < word.length(); i++) {
                int iPos = s.indexOf(word.charAt(i), iUltPostion + 1);
                if (iPos != -1) {
                    iUltPostion = iPos;
                } else {
                    bControl = false;
                    break;
                }
            }
            if (bControl)
                iOutput++;
        }
        return iOutput;
    }
}
