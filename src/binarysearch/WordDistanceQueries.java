package binarysearch;

import java.util.ArrayList;
import java.util.Arrays;

public class WordDistanceQueries {
    private ArrayList<String> aOutputs;

    public static void main(String[] args) {
        WordDistanceQueries obj = new WordDistanceQueries();
        obj.WordDistanceQuerierM(new String[]{"a", "b", "c", "d", "a"/*, "d", "e", "c", "a", "w"*/});
        System.out.println(obj.distance("a", "c"));
    }

    public void WordDistanceQuerierM(String[] words) {
        this.aOutputs = new ArrayList<>(Arrays.asList(words));
    }

    public int distance(String a, String b) {
        int iMenor = 999999;
        int iPosition=this.aOutputs.indexOf(a);
        while(iPosition!=-1)
        {
            int iLeft = this.aOutputs.subList(0, iPosition + 1).lastIndexOf(b);
            int iRight = this.aOutputs.subList(iPosition, this.aOutputs.size()).indexOf(b) + iPosition;
            iLeft = (iLeft != -1) ? iPosition - iLeft : -1;
            iRight = (iRight != -1) ? iRight - iPosition : -1;
            if (iLeft != -1 && iRight != -1) {
                int iMin = Math.min(iLeft, iRight);
                iMenor = Math.min(iMin, iMenor);
            } else if (iLeft != -1) {
                iMenor = Math.min(iLeft, iMenor);
            } else if (iRight != -1) {
                iMenor = Math.min(iRight, iMenor);
            }
            this.aOutputs.set(iPosition,"-");
            iPosition=this.aOutputs.indexOf(a);
        }
        return iMenor;
    }
}
