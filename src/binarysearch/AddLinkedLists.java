package binarysearch;

import java.util.ArrayList;

public class AddLinkedLists {
    public static void main(String[] args) {
        AddLinkedLists v = new AddLinkedLists();
        v.solve();
    }

    public LLNode solve() {
        System.out.println(sum("4984", "1234"));
        return null;
    }

    public String sum(String sNumero1, String sNumero2) {
        int iSizeNumber1 = sNumero1.length();
        int iSizeNumber2 = sNumero2.length();
        int iEspacios = (Math.max(iSizeNumber1, iSizeNumber2) - Math.min(iSizeNumber1, iSizeNumber2));
        StringBuilder sNewNumber = new StringBuilder();
        if (iSizeNumber1 > iSizeNumber2)
            sNumero2 = acompletar(sNumero2, iEspacios);
        else
            sNumero1 = acompletar(sNumero1, iEspacios);
        int iAcarreo = 0;
        for (int i = sNumero1.length() - 1; i > -1; i--) {
            int iValue1 = Integer.parseInt(sNumero1.charAt(i) + "");
            int iValue2 = Integer.parseInt(sNumero2.charAt(i) + "");
            int iSuma = iValue1 + iValue2 + iAcarreo;
            if (iSuma >= 10) {
                iAcarreo = 1;
                sNewNumber.insert(0, iSuma % 10);
            } else {
                iAcarreo = 0;
                sNewNumber.insert(0, iSuma);
            }
        }
        return sNewNumber.toString();
    }

    public String acompletar(String sNumero, int iEspacios) {
        StringBuilder sNewNumber = new StringBuilder(sNumero);
        for (int i = 0; i < iEspacios; i++) {
            sNewNumber.insert(0,'0');
        }
        return sNewNumber.toString();
    }

    class LLNode {
        int val;
        LLNode next;
    }
}
