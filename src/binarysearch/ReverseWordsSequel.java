/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearch;

/**
 *
 * @author Daniel Ochoa
 */
public class ReverseWordsSequel {

    public void ReverseWordsSequel() {
        String sentence = "hello//world:here";
        String[] delimiters = {"/", ":"};
        StringBuilder sSplit = new StringBuilder();
        StringBuilder sCharacters = new StringBuilder();
        for (String delimiter : delimiters) {
            sSplit.append(delimiter);
        }
        String sArr[] = sentence.split("[" + sSplit.toString() + "]");
        sSplit = new StringBuilder(sentence);
        for (String delimiter : delimiters) {
            int iSearch = sSplit.indexOf(delimiter);
            while (iSearch != -1) {
                sSplit.setCharAt(iSearch, 'A');
                sCharacters.append(delimiter);
                iSearch = sSplit.indexOf(delimiter);
            }
        }
        sSplit = new StringBuilder();
        for (int i = sArr.length - 1; i > -1; i--) {
            sSplit.append(sArr[i]);
            if (sCharacters.length() > 0 && sArr[i].length() != 0) {
                sSplit.append(("" + sCharacters.charAt(0)));
                sCharacters.deleteCharAt(0);
            }
            if (sArr[i].length() == 0) {
                sSplit.append(("" + sCharacters.charAt(0)));
                sCharacters.deleteCharAt(0);
            }
        }
        for (int i = 0; i < sCharacters.length(); i++) {
            sSplit.append(("" + sCharacters.charAt(0)));
        }
        System.out.println(sSplit.toString());
    }
}
