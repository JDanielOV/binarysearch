/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearch;

import java.util.ArrayList;
import java.util.Collections;

public class Flipped_Matrix_Prequel {

    public void Flipped_Matrix_Prequel() {
        int[][] matrix
                = {
                    {1, 0, 1},
                    {0, 1, 0},
                    {1, 0, 0},
                    {1, 0, 1}
                };
        ArrayList<counterOnes> lRows = new ArrayList();
        ArrayList<counterOnes> lColumns = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            int iCounter = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == lColumns.size()) {
                    if (matrix[i][j] == 1) {
                        lColumns.add(new counterOnes(j, 1));
                    } else {
                        lColumns.add(new counterOnes(j, 0));
                    }
                } else {
                    if (matrix[i][j] == 1) {
                        lColumns.get(j).setiCounter(lColumns.get(j).getiCounter() + 1);
                    }
                }
                if (matrix[i][j] == 1) {
                    iCounter++;
                }
            }
            lRows.add(new counterOnes(i, iCounter));
        }
        Collections.sort(lRows);
        int j = lRows.get(0).getiPosition();
        int iNewOnes = 0;
        for (int i = 0; i < matrix.length; i++) {
            int iCounter = 0;
            for (int k = 0; k < matrix[i].length; k++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
        for (counterOnes lColumn : lColumns) {
            iNewOnes += lColumn.getiCounter();
        }
        for (int i = 0; i < matrix[j].length; i++) {
            if (matrix[j][i] == 0) {
                matrix[j][i] = 1;
                iNewOnes++;
                lColumns.get(i).setiCounter(lColumns.get(i).getiCounter() + 1);
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            int iCounter = 0;
            for (int k = 0; k < matrix[i].length; k++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
        Collections.sort(lColumns);
        j = lColumns.get(0).getiPosition();
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][j] == 0) {
                iNewOnes++;
                matrix[i][j] = 1;
            }
        }
        System.out.println(iNewOnes);
        for (int i = 0; i < matrix.length; i++) {
            int iCounter = 0;
            for (int k = 0; k < matrix[i].length; k++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public class counterOnes implements Comparable<counterOnes> {

        public int iPosition = 0;
        public int iCounter = 0;

        public counterOnes(int iPosition, int iCounter) {
            this.iPosition = iPosition;
            this.iCounter = iCounter;
        }

        public int getiPosition() {
            return iPosition;
        }

        public void setiPosition(int iPosition) {
            this.iPosition = iPosition;
        }

        public int getiCounter() {
            return iCounter;
        }

        public void setiCounter(int iCounter) {
            this.iCounter = iCounter;
        }

        @Override
        public int compareTo(counterOnes otro) {
            return this.getiCounter() - otro.getiCounter();
        }
    }
}
