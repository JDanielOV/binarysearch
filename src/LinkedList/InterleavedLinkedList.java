package LinkedList;

public class InterleavedLinkedList {

    public Nodo nHead1() {
        int iArr[] = {4, 5, 6, 7,8,9};
        Nodo nHead = new Nodo();
        Nodo nReturn = new Nodo();
        for (int i = 0; i < iArr.length; i++) {
            Nodo nTemp = new Nodo();
            nTemp.val = iArr[i];
            if (nHead == null) {
                nReturn = nHead = nTemp;
            } else {
                Nodo search = nHead;
                while (search.next != null) {
                    search = search.next;
                }
                search.next = nTemp;
                nReturn = search;
            }
        }
        return nReturn;
    }

    public void InterleavedLinkedList() {
        Nodo nHead1 = nHead1();
        while (nHead1.next != null) {
            System.out.println(nHead1.val);
            nHead1 = nHead1.next;
        }
        System.out.println(nHead1.val);
    }
}
